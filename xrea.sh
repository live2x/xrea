#!/bin/bash

cd ~
basepath=$(cd `dirname $0`;pwd)

echo 'export PATH=$PATH:~/bin' > ~/.bash_profile

cat>>~/.bash_profile<<EOF

# Change php version
php_bin="php72"
php_cfg="~/public_html/.fast-cgi-bin/\${php_bin}.ini"
php_cli="\${php_bin}cli"

# Add alias
alias php="\${php_bin} -c \${php_cfg}"
alias phpcli="\${php_cli} -c \${php-cfg}"
EOF

cat>>~/public_html/.fast-cgi-bin/php72.ini<<EOF
zend_extension=php72_opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.save_comments=1
opcache.revalidate_freq=1
EOF

cd ~

mkdir bin
cd ~/bin

# Download composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar composer

# Add alias
cat>>~/.bash_profile<<EOF
alias composer="phpcli ~/bin/composer"
EOF

cd ${basepath}
source ~/.bash_profile

cat>>~/.vimrc<<EOF
set fileencodings=utf-8,gb2312,gbk,gb18030
set termencoding=utf-8
set fileformats=unix
set encoding=utf-8
EOF

# install nodejs
wget https://nodejs.org/dist/v12.16.0/node-v12.16.0-linux-x64.tar.xz
tar -xvf node-v12.16.0-linux-x64.tar.xz
rm -f node-v12.16.0-linux-x64.tar.xz
mv node-v12.16.0-linux-x64 nodejs

# Add alias
cat>>~/.bash_profile<<EOF
alias node="~/nodejs/bin/node"
alias npm="node ~/nodejs/bin/npm"
alias npx="node ~/nodejs/bin/npx"
EOF

source ~/.bash_profile

# Set nodejs config directory
npm set prefix="~/nodejs"

# install yarn
npm install -g yarn
cat>>~/.bash_profile<<EOF
alias yarn="node ~/nodejs/bin/yarn"
alias yarnpkg="node ~/nodejs/bin/yarnpkg"
EOF

source ~/.bash_profile